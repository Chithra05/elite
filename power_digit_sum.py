def power_digit_sum():
    num = str(2 ** 1000)
    digits = [int(x) for x in num]
    return sum(digits)

print(power_digit_sum())