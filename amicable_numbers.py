def amicable_numbers():
    res = 0
    for i in range(1, 10000):
        if is_amicable(i):
            res += i
    return res

def is_amicable(i):
    p = sum(find_divisors(i))
    q = sum(find_divisors(p))
    return q == i

def find_divisors(num):
    divisors = []
    for i in range(1, num // 2 + 1):
        if num % i == 0:
            divisors.append(i)
    return divisors

print(amicable_numbers())