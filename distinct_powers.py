def distinct_powers():
    set1 = set()
    for i in range(2, 101):
        for j in range(2, 101):
            set1.add(i ** j)
    return len(set1)

print(distinct_powers())