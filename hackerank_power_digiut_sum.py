t = int(input())
for tc in range(t):
    n = int(input())
    s = str(2 ** n)
    l = [int(i) for i in s]
    print(sum(l))